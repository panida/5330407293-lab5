<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>RDF_Book</title>
            </head>
            <body>
                <table border="1">
                    <tr>
                        <th>Title</th><th>Pages</th>
                    </tr>
                    <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort data-type="number" select="lib:pages"/>
                        <xsl:if test="lib:pages = true()">
                        <tr>
                            <td><xsl:value-of select="@about"/></td>
                            <td><xsl:value-of select="lib:pages"/></td>
                        </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
