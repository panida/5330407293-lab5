<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:kml2="http://www.opengis.net/kml/2.2"
                xmlns:atom="http://www.w3.org/2005/Atom">
    <xsl:output method="html"/>    
    <xsl:template match="/"> 
    <html>
        <head>
                <title>Hotel List</title>
            </head>
            <body>
                <h1>
                    <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select = "kml2:kml/kml2:Document/kml2:Style/kml2:IconStyle/kml2:Icon/kml2:href"/>
                        </xsl:attribute>                        
                        <xsl:value-of select = "kml2:kml/kml2:Document/kml2:name"/>
                    </img>
                </h1>
                <p>List of hotels                    
                    <xsl:for-each select="kml2:kml/kml2:Document/kml2:Placemark">
                        <xsl:sort data-type="text" select="kml2:name"/>
                    <ul>
                        <li>                            
                            <xsl:value-of select = "kml2:name"/>                            
                            <ul>
                                <li>
                                    <xsl:variable name="Url">
                                        <xsl:value-of select = "kml2:description"/>
                                    </xsl:variable>
                                    <a href="{substring-after(substring-before($Url,'&quot;&gt;&lt;'),'&lt;a href=&quot;')}">
                                        <xsl:value-of select = "substring-after(substring-before($Url,'&quot;&gt;&lt;'),'&lt;a href=&quot;')"/>
                                    </a>
                                </li>
                                <li>
                                    Coordinates : <xsl:value-of select="kml2:Point/kml2:coordinates"/>
                                </li>
                            </ul>
                        </li>                        
                    </ul>
                    </xsl:for-each>                    
                </p>                
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
